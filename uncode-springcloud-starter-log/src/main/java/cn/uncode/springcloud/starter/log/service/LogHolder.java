package cn.uncode.springcloud.starter.log.service;

/**
 * 自定义内容存储
 * @author juny
 * @date 2019年4月23日
 *
 */
public class LogHolder {

    private static final ThreadLocal<String> LOG_CONTEXT_TRACE_ID = new ThreadLocal<>();
    private static final ThreadLocal<Long> LOG_CONTEXT_USER_ID = new ThreadLocal<>();
    private static final ThreadLocal<String> LOG_CONTEXT_LOG_ANNOTATION = new ThreadLocal<>();

    public static void setTraceId(String traceId) {
    	LOG_CONTEXT_TRACE_ID.set(traceId);
    }

    public static String getTraceId() {
        return LOG_CONTEXT_TRACE_ID.get();
    }
    
    public static void setValueOfLogAnnotation(String value) {
    	LOG_CONTEXT_LOG_ANNOTATION.set(value);
    }

    public static String getValueOfLogAnnotation() {
        return LOG_CONTEXT_LOG_ANNOTATION.get();
    }
    
    public static void setUserId(long userId) {
    	LOG_CONTEXT_USER_ID.set(userId);
    }

    public static long getUserId() {
    	if(null != LOG_CONTEXT_USER_ID.get()) {
    		return LOG_CONTEXT_USER_ID.get();
    	}
        return 0;
    }

    public static void remove() {
    	LOG_CONTEXT_TRACE_ID.remove();
    	LOG_CONTEXT_USER_ID.remove();
    	LOG_CONTEXT_LOG_ANNOTATION.remove();
    }
}
