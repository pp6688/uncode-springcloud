/*
Navicat MySQL Data Transfer

Source Server         : 10.1.41.32
Source Server Version : 50626
Source Host           : 10.1.41.32:3306
Source Database       : uncode_admin

Target Server Type    : MYSQL
Target Server Version : 50626
File Encoding         : 65001

Date: 2019-07-04 18:31:51
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `canary`
-- ----------------------------
DROP TABLE IF EXISTS `canary`;
CREATE TABLE `canary` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键ID',
  `name` varchar(100) DEFAULT '' COMMENT '灰度名称',
  `flag` varchar(50) DEFAULT '' COMMENT '灰度标识',
  `type` varchar(20) DEFAULT 'ip' COMMENT '类型（ip:网址，keyInRequest:请求标识，keyInSession:会话标识）',
  `key` varchar(50) NOT NULL DEFAULT '' COMMENT '灰度关键字',
  `value` varchar(50) NOT NULL DEFAULT '' COMMENT '灰度值',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(500) DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=104 DEFAULT CHARSET=utf8 COMMENT='灰度配置表';

-- ----------------------------
-- Records of canary
-- ----------------------------
INSERT INTO `canary` VALUES ('102', 'UNCODE-ADMIN', 'dev', 'ip', '', '192.168.17.163', '2019-06-04 11:09:51', '2019-06-04 11:12:14', '');

-- ----------------------------
-- Table structure for `config`
-- ----------------------------
DROP TABLE IF EXISTS `config`;
CREATE TABLE `config` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键ID',
  `name` varchar(100) DEFAULT '' COMMENT '参数名称',
  `key` varchar(100) DEFAULT '' COMMENT '参数键名',
  `value` varchar(500) DEFAULT NULL COMMENT '参数键值',
  `type` tinyint(2) NOT NULL DEFAULT '1' COMMENT '状态（1:系统级，2：功能级，3：其他）',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  `remark` varchar(500) DEFAULT NULL COMMENT '备注',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COMMENT='参数配置表';

-- ----------------------------
-- Records of config
-- ----------------------------
INSERT INTO `config` VALUES ('1', '网关发布版本', 'gateway_version', '1231222,1559548615193,1559548651855,1559548698030,1559553153072,1562234119506,1562234125339', '1', null, '2019-07-04 17:55:25', null);

-- ----------------------------
-- Table structure for `desk_icon`
-- ----------------------------
DROP TABLE IF EXISTS `desk_icon`;
CREATE TABLE `desk_icon` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键ID',
  `name` varchar(100) NOT NULL COMMENT '桌面图标名字',
  `url` varchar(100) NOT NULL COMMENT '图标打开窗口的请求地址',
  `open_type` int(11) NOT NULL DEFAULT '2' COMMENT '窗口类型',
  `title` varchar(100) NOT NULL COMMENT '窗口标题',
  `icon` varchar(100) NOT NULL COMMENT '桌面图标路径',
  `status` int(11) NOT NULL DEFAULT '1' COMMENT '状态（1:在用，2：禁用，3：待审核）',
  `create_time` datetime NOT NULL COMMENT '创建时间',
  `pid` bigint(20) NOT NULL DEFAULT '0' COMMENT '父ID',
  `menu_type` int(11) NOT NULL DEFAULT '1' COMMENT '菜单类型',
  `order` int(3) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1908 DEFAULT CHARSET=utf8 COMMENT='桌面图标表';

-- ----------------------------
-- Records of desk_icon
-- ----------------------------
INSERT INTO `desk_icon` VALUES ('1', '主题', 'theme', '2', '主题', 'fa-television', '2', '2019-05-15 09:41:52', '0', '1', '0');
INSERT INTO `desk_icon` VALUES ('2', 'Eureka', 'views/eureka/list_iframe.html', '2', 'Eureka', 'fa-leaf', '1', '2019-05-15 09:41:52', '0', '1', '0');
INSERT INTO `desk_icon` VALUES ('3', 'Jenkins', 'http://10.1.41.189:9000/', '3', 'Jenkins', 'fa-connectdevelop', '1', '2019-05-15 09:41:52', '0', '1', '0');
INSERT INTO `desk_icon` VALUES ('5', '系统菜单', 'views/menu/list_iframe.html', '2', '系统菜单', 'fa-navicon', '2', '2019-05-15 09:41:52', '0', '1', '0');
INSERT INTO `desk_icon` VALUES ('6', '角色管理', 'views/role/list.html', '2', '角色管理', 'fa-th-large', '2', '2019-05-15 09:41:52', '0', '1', '0');
INSERT INTO `desk_icon` VALUES ('7', '点赞', 'http://fly.layui.com/case/u/3547992', '2', '点赞', 'fa-thumbs-up', '2', '2019-05-15 09:41:52', '0', '1', '0');
INSERT INTO `desk_icon` VALUES ('8', 'ECharts', 'views/echarts/demo.html', '2', 'ECharts', 'fa-bar-chart', '2', '2019-05-15 09:41:52', '0', '1', '0');
INSERT INTO `desk_icon` VALUES ('9', 'Layui框架', 'http://www.layui.com/', '2', 'Layui', 'http://www.layui.com/favicon.ico', '2', '2019-05-15 09:41:52', '0', '1', '0');
INSERT INTO `desk_icon` VALUES ('10', 'Swagger', 'http://10.1.41.236:2000/doc.html', '2', 'Swagger', 'fa-user-circle', '1', '2019-05-15 09:41:52', '0', '1', '0');
INSERT INTO `desk_icon` VALUES ('11', '主题', 'theme', '1', '主题', 'fa-television', '1', '2019-05-15 09:41:52', '0', '2', '0');
INSERT INTO `desk_icon` VALUES ('12', '打赏作者', '/home/reward', '1', '打赏作者', 'fa-qrcode', '2', '2019-05-15 09:41:52', '0', '2', '0');
INSERT INTO `desk_icon` VALUES ('13', '基本说明', 'theme', '2', '基本说明', 'fa-television', '2', '2019-05-15 09:41:52', '0', '2', '0');
INSERT INTO `desk_icon` VALUES ('14', '点赞', 'http://fly.layui.com/case/u/3547992', '2', '点赞', 'fa-thumbs-up', '2', '2019-05-15 09:41:52', '0', '2', '0');
INSERT INTO `desk_icon` VALUES ('15', 'WinAdmin在线文档', 'http://www.win-ui.com/doc', '2', 'WinAdmin在线文档', 'fa-thumbs-up', '1', '2019-05-15 09:41:52', '0', '2', '0');
INSERT INTO `desk_icon` VALUES ('16', '系统设置', '', '1', '系统设置', 'fa-cog', '1', '2019-05-15 09:41:52', '0', '2', '0');
INSERT INTO `desk_icon` VALUES ('17', 'Font Awesome图标展示', '', '1', 'Font Awesome图标展示', 'fa-user', '1', '2019-05-15 09:41:52', '0', '2', '0');
INSERT INTO `desk_icon` VALUES ('18', 'Font Awesome第三方LOGO', '', '1', 'Font Awesome第三方LOGO', 'fa-edge', '1', '2019-05-15 09:41:52', '0', '2', '0');
INSERT INTO `desk_icon` VALUES ('19', '自定义图片菜单', '', '1', '自定义图片菜单', 'images/logo_100.png', '1', '2019-05-15 09:41:52', '0', '2', '0');
INSERT INTO `desk_icon` VALUES ('20', 'Cat监控', 'http://10.1.41.230:8080/cat/r', '2', 'Cat监控', 'fa-tachometer', '1', '2019-05-15 09:41:52', '0', '1', '0');
INSERT INTO `desk_icon` VALUES ('21', 'Apollo', 'http://10.1.41.230:8070/signin', '3', 'Apollo', 'fa fa-cogs', '1', '2019-05-15 09:41:52', '0', '1', '0');
INSERT INTO `desk_icon` VALUES ('22', 'Sentinel', 'http://10.1.41.102:8102/#/dashboard/home', '2', 'Sentinel', 'fa-heartbeat', '1', '2019-05-15 09:41:52', '0', '1', '0');
INSERT INTO `desk_icon` VALUES ('23', 'Gitlib', 'http://git.cjbnb.com/', '3', 'Gitlib', 'fa-gitlab', '1', '2019-05-15 09:41:52', '0', '1', '0');
INSERT INTO `desk_icon` VALUES ('24', 'Gateway', 'views/gateway/gateway_iframe.html', '2', 'Gateway', 'fa-microchip', '1', '2019-05-15 09:41:52', '0', '1', '0');
INSERT INTO `desk_icon` VALUES ('25', '灰度发布', 'views/canary/list_iframe.html', '2', '灰度发布配置', 'fa-anchor', '1', '2019-05-15 09:41:52', '0', '1', '0');
INSERT INTO `desk_icon` VALUES ('26', '网关路由', 'views/gateway/list_iframe.html', '2', '网关路由配置', 'fa-random', '1', '2019-05-15 09:41:52', '0', '1', '0');
INSERT INTO `desk_icon` VALUES ('27', 'Cache', 'views/cache/list_iframe.html', '2', 'Uncode-Cache', 'fa-hdd-o', '1', '2019-05-15 09:41:52', '0', '1', '0');
INSERT INTO `desk_icon` VALUES ('30', '菜单管理', 'views/menus/list_iframe.html', '2', '系统管理', 'fa-navicon', '1', '2019-05-15 09:41:52', '0', '2', '0');
INSERT INTO `desk_icon` VALUES ('31', '操作员管理', '/operator/index', '1', '操作员管理', 'fa-user', '1', '2019-05-15 09:41:52', '0', '2', '0');
INSERT INTO `desk_icon` VALUES ('32', '角色管理', 'views/role/list.html', '2', '角色管理', 'fa-th-large', '1', '2019-05-15 09:41:52', '0', '2', '0');
INSERT INTO `desk_icon` VALUES ('1601', '系统菜单', 'views/menu/list_iframe.html', '2', '系统菜单', 'fa-navicon', '1', '2019-05-15 09:41:52', '16', '2', '0');
INSERT INTO `desk_icon` VALUES ('1602', '操作员管理', '/operator/index', '1', '操作员管理', 'fa-user', '1', '2019-05-15 09:41:52', '16', '2', '0');
INSERT INTO `desk_icon` VALUES ('1603', '角色管理', 'views/role/list.html', '2', '角色管理', 'fa-th-large', '1', '2019-05-15 09:41:52', '16', '2', '0');
INSERT INTO `desk_icon` VALUES ('1701', '图片', 'http://www.fontawesome.com.cn/', '2', '图片', 'fa-photo', '1', '2019-05-15 09:41:52', '17', '2', '0');
INSERT INTO `desk_icon` VALUES ('1702', '书签', 'http://www.fontawesome.com.cn/', '2', '书签', 'fa-bookmark', '1', '2019-05-15 09:41:52', '17', '2', '0');
INSERT INTO `desk_icon` VALUES ('1703', '日历', 'http://www.fontawesome.com.cn/', '2', '日历', 'fa-calendar', '1', '2019-05-15 09:41:52', '17', '2', '0');
INSERT INTO `desk_icon` VALUES ('1704', '评论', 'http://www.fontawesome.com.cn/', '2', '评论', 'fa-comments', '1', '2019-05-15 09:41:52', '17', '2', '0');
INSERT INTO `desk_icon` VALUES ('1705', '邮件', 'http://www.fontawesome.com.cn/', '2', '邮件', 'fa-envelope', '1', '2019-05-15 09:41:52', '17', '2', '0');
INSERT INTO `desk_icon` VALUES ('1706', '桌面', 'http://www.fontawesome.com.cn/', '2', '桌面', 'fa-desktop', '1', '2019-05-15 09:41:52', '17', '2', '0');
INSERT INTO `desk_icon` VALUES ('1707', '下载', 'http://www.fontawesome.com.cn/', '2', '下载', 'fa-download', '1', '2019-05-15 09:41:52', '17', '2', '0');
INSERT INTO `desk_icon` VALUES ('1708', '设置', 'http://www.fontawesome.com.cn/', '2', '设置', 'fa-cog', '1', '2019-05-15 09:41:52', '17', '2', '0');
INSERT INTO `desk_icon` VALUES ('1709', '信息', 'http://www.fontawesome.com.cn/', '2', '信息', 'fa-info-circle', '1', '2019-05-15 09:41:52', '17', '2', '0');
INSERT INTO `desk_icon` VALUES ('1710', '图表1', 'http://www.fontawesome.com.cn/faicons/#chart', '2', '图表1', 'fa-line-chart', '1', '2019-05-15 09:41:52', '17', '2', '0');
INSERT INTO `desk_icon` VALUES ('1711', '图表2', 'http://www.fontawesome.com.cn/faicons/#chart', '2', '图表2', 'fa-bar-chart', '1', '2019-05-15 09:41:52', '17', '2', '0');
INSERT INTO `desk_icon` VALUES ('1712', '图表3', 'http://www.fontawesome.com.cn/faicons/#chart', '2', '图表3', 'fa-pie-chart', '1', '2019-05-15 09:41:52', '17', '2', '0');
INSERT INTO `desk_icon` VALUES ('1713', '标签', 'http://www.fontawesome.com.cn/', '2', '标签', 'fa-tags', '1', '2019-05-15 09:41:52', '17', '2', '0');
INSERT INTO `desk_icon` VALUES ('1714', '垃圾', 'http://www.fontawesome.com.cn/', '2', '垃圾', 'fa-trash', '1', '2019-05-15 09:41:52', '17', '2', '0');
INSERT INTO `desk_icon` VALUES ('1715', '医疗', 'http://www.fontawesome.com.cn/', '2', '医疗', 'fa-medkit', '1', '2019-05-15 09:41:52', '17', '2', '0');
INSERT INTO `desk_icon` VALUES ('1801', '苹果', 'https://www.flaticon.com/packs', '2', '苹果', 'fa-apple', '1', '2019-05-15 09:41:52', '18', '2', '0');
INSERT INTO `desk_icon` VALUES ('1802', 'Google Chrome', 'https://www.flaticon.com/packs', '2', 'Google Chrome', 'fa-chrome', '1', '2019-05-15 09:41:52', '18', '2', '0');
INSERT INTO `desk_icon` VALUES ('1803', 'Microsoft Edge', 'https://www.flaticon.com/packs', '2', 'Microsoft Edge', 'fa-edge', '1', '2019-05-15 09:41:52', '18', '2', '0');
INSERT INTO `desk_icon` VALUES ('1804', '腾讯QQ', 'https://www.flaticon.com/packs', '2', '腾讯QQ', 'fa-qq', '1', '2019-05-15 09:41:52', '18', '2', '0');
INSERT INTO `desk_icon` VALUES ('1805', '新浪微博', 'https://www.flaticon.com/packs', '2', '新浪微博', 'fa-weibo', '1', '2019-05-15 09:41:52', '18', '2', '0');
INSERT INTO `desk_icon` VALUES ('1806', 'Microsoft Windows', 'https://www.flaticon.com/packs', '2', 'Microsoft Windows', 'fa-windows', '1', '2019-05-15 09:41:52', '18', '2', '0');
INSERT INTO `desk_icon` VALUES ('1807', '微信', 'https://www.flaticon.com/packs', '2', '微信', 'fa-wechat', '1', '2019-05-15 09:41:52', '18', '2', '0');
INSERT INTO `desk_icon` VALUES ('1808', 'Skype', 'https://www.flaticon.com/packs', '2', 'Skype', 'fa-skype', '1', '2019-05-15 09:41:52', '18', '2', '0');
INSERT INTO `desk_icon` VALUES ('1809', 'Github', 'https://www.flaticon.com/packs', '2', 'Github', 'fa-github', '1', '2019-05-15 09:41:52', '18', '2', '0');
INSERT INTO `desk_icon` VALUES ('1901', '腾讯QQ', '', '1', '腾讯QQ', 'images/qq_32.png', '1', '2019-05-15 09:41:52', '19', '2', '0');
INSERT INTO `desk_icon` VALUES ('1902', '新浪微博', '', '1', '新浪微博', 'images/weibo_32.png', '1', '2019-05-15 09:41:52', '19', '2', '0');
INSERT INTO `desk_icon` VALUES ('1903', 'QQ空间', '', '1', 'QQ空间', 'images/qzone_32.png', '1', '2019-05-15 09:41:52', '19', '2', '0');
INSERT INTO `desk_icon` VALUES ('1905', 'Wiki', 'http://wiki.cjbnb.com/doku.php', '2', 'Wiki', 'fa-wikipedia-w', '1', '2019-06-06 16:24:05', '0', '1', '2');
INSERT INTO `desk_icon` VALUES ('1906', '监控', 'http://127.0.0.1:8000/sbadmin#/wallboard', '2', 'Spring Boot Admin', 'fa-heartbeat', '1', '2019-06-10 16:16:02', '0', '1', '1');
INSERT INTO `desk_icon` VALUES ('1907', 'Task', 'views/task/homepage.html', '2', '任务调度', 'fa-tasks', '1', '2019-06-25 10:53:01', '0', '1', '0');

-- ----------------------------
-- Table structure for `gateway_route`
-- ----------------------------
DROP TABLE IF EXISTS `gateway_route`;
CREATE TABLE `gateway_route` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键ID',
  `route_id` varchar(64) DEFAULT NULL COMMENT '路由id',
  `route_uri` varchar(128) DEFAULT NULL COMMENT '转发目标uri',
  `route_order` int(11) DEFAULT NULL COMMENT '路由执行顺序',
  `predicates` text COMMENT '断言字符串集合，字符串结构：[{"name":"Path","args":{"pattern" : "/zy/**"}}]',
  `filters` text COMMENT '过滤器字符串集合，字符串结构：{"name":"StripPrefix","args":{"_genkey_0":"1"}}',
  `status` int(2) NOT NULL DEFAULT '1' COMMENT '状态（1:启用，2：禁用，3：删除）',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_time` datetime DEFAULT NULL COMMENT '修改时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COMMENT='动态路由表';

-- ----------------------------
-- Records of gateway_route
-- ----------------------------
INSERT INTO `gateway_route` VALUES ('1', 'baidu', 'http://baidu.com:80/', '0', '[{\"name\":\"Path\",\"args\":{\"pattern\":\"/baidu\"}}]', null, '2', '2019-05-30 13:45:46', '2019-06-03 10:19:51');
INSERT INTO `gateway_route` VALUES ('2', 'rrrrw555', 'rrr66666', '0', '', '', '3', '2019-05-31 17:08:26', '2019-05-31 17:53:32');

-- ----------------------------
-- Table structure for `user`
-- ----------------------------
DROP TABLE IF EXISTS `user`;
CREATE TABLE `user` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT '主键ID',
  `account` varchar(36) NOT NULL COMMENT '账户',
  `password` varchar(32) NOT NULL COMMENT '密码',
  `name` varchar(32) NOT NULL COMMENT '姓名',
  `email` varchar(64) DEFAULT NULL COMMENT '电子邮箱',
  `mobile` varchar(20) DEFAULT NULL COMMENT '手机号码',
  `wx` varchar(32) DEFAULT NULL COMMENT '微信号',
  `qq` varchar(16) DEFAULT NULL COMMENT 'QQ号',
  `remark` varchar(512) DEFAULT NULL COMMENT '描述信息',
  PRIMARY KEY (`id`),
  UNIQUE KEY `user_account` (`account`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='系统用户';

-- ----------------------------
-- Records of user
-- ----------------------------
